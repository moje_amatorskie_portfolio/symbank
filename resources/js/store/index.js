import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    state: {
        selectedAccountNumber: "",
        message: "",  //wiadomość wyświetlana po powrocie z api przez ajax
        messageClass: "",
    },
    mutations: {
        setAccountNumber(currentState, selectedAccountNumber) {
            currentState.selectedAccountNumber = selectedAccountNumber;
        },
        setMessage(currentState, message) {
            currentState.message = message;
        },
        setMessageClass(currentState, messageClass) {
            currentState.messageClass = messageClass;
        },
    }
})
