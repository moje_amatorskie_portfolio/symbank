@extends('layouts.app')

@section('custom_css')
<link href="{{ asset('custom_home/css/blog-home.css') }}" rel="stylesheet">
@endsection

@section('content')
<main-bank-component translation="{{  File::get(resource_path('lang/'.config('app.locale').'.json')) }}">
</main-bank-component>
@endsection
