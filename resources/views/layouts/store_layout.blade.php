@extends('layouts.app')
<!--zduplikowalem kod  widoku logowania blade i zrobiłem własne logowanie poniewaz middleware auth przekierowywało po zalogowaniu z powrotem do sklepu zamiast na strone platnosci banku -->
@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ ('custom_login/css/css.css') }}">
@endsection

@section('content')
<div class="login-wrap">
    <div class="login-html">

@yield('box')

    </div>
</div>
@endsection
