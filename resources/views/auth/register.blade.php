@extends('layouts.app')

@section('custom_css')
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{ asset('custom_register/images/icons/favicon.ico') }}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/vendor/bootstrap/css/bootstrap.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/fonts/iconic/css/material-design-iconic-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/vendor/animate/animate.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/vendor/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/vendor/animsition/css/animsition.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/vendor/select2/select2.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/vendor/daterangepicker/daterangepicker.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('custom_register/css/main.css') }}">
<!--===============================================================================================-->
@endsection

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="login100-more" style="background-image: url('{{ asset('custom_register/images/bg-01.jpg') }}');"></div>

        <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
            <form class="login100-form validate-form" method="POST" action="{{ route('register') }}">
                @csrf

                <span class="login100-form-title p-b-59">
                    {{ __('Register') }}
                </span>

                @error('name')
                    <span role="alert">
                        <strong class="text-danger h4">{{ $message }}</strong>
                    </span>
                @enderror

                @error('email')
                    <span role="alert">
                        <strong class="text-danger h4">{{ $message }}</strong>
                    </span>
                @enderror

                @error('password')
                    <span role="alert">
                        <strong class="text-danger h4">{{ $message }}</strong>
                    </span>
                @enderror

                @error('username')
                    <span role="alert">
                        <strong class="text-danger h4">{{ $message }}</strong>
                    </span>
                @enderror

                <div class="wrap-input100 validate-input" data-validate="Name is required">
                    <span class="label-input100">{{ __('Name') }}</span>
                    <input class="input100" type="text" name="name" value="{{ old('name') }}" placeholder="{{ __('Name') }}">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                    <span class="label-input100">{{ __('E-Mail Address') }}</span>
                    <input class="input100" type="text" name="email" placeholder="{{ __('E-Mail Address') }}">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Username is required">
                    <span class="label-input100">{{ __('Customer Id') }} (number min 3)</span>
                    <input class="input100" type="text" name="username" placeholder="{{ __('Customer_Id') }}">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Password is required">
                    <span class="label-input100">{{ __('Password') }} (min 3)</span>
                    <input class="input100" type="password" name="password" placeholder="*************">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Repeat Password is required">
                    <span class="label-input100">{{ __('Confirm Password') }}  (min 3)</span>
                    <input class="input100" type="password" name="password_confirmation" placeholder="*************">
                    <span class="focus-input100"></span>
                </div>

                <div class="flex-m w-full p-b-33">
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" checked disabled name="remember-me">
                        <label class="label-checkbox100" for="ckb1">
                            <span class="txt1">
                                {{ __('I_Agree') }}
                                <a href="#" class="txt2 hov1">
                                    {{ __('Terms_Conditions') }}
                                </a>
                            </span>
                        </label>
                    </div>

                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            {{ __('Register') }}
                        </button>
                    </div>

                    <a href="{{ route('login') }}" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
                        {{ __('Login') }}
                        <i class="fa fa-long-arrow-right m-l-5"></i>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<!--===============================================================================================-->
	<script src="{{ asset('custom_register/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('custom_register/vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('custom_register/vendor/bootstrap/js/popper.js') }}"></script>
	<script src="{{ asset('custom_register/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('custom_register/vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('custom_register/vendor/daterangepicker/moment.min.js') }}"></script>
	<script src="{{ asset('custom_register/vendor/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('custom_register/vendor/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('custom_register/js/main.js') }}"></script>
@endsection
