@extends('layouts.store_layout')

@section('box')
<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab text-center">{{ __('Login') }}:111 {{ __('Password') }}:111 <br />{{ __('Or') }}</br>{{ __('Login') }}:222 {{ __('Password') }}:222</label>
<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab text-center"></label>
<div class="login-form">
    <div class="sign-in-htm">
        <form method="POST" action="{{url('store_payment_authenticate')}}" autocomplete="off">
            @csrf

            <div class="group">
                <label for="username" class="label">{{ __('Customer_Id') }}</label>
                <input id="username" type="text" class="input" name="username" value="111" autocomplete="off">
                <span role="alert">
                    <strong class="text-danger h4">{{ $message ?? "" }}</strong>
                </span>
            </div>

            <div class="group">
                <label for="pass" class="label">{{ __('Password') }}</label>
                <input id="pass" type="password" class="input" data-type="password" name="password" value="111">
                @error('password')
                <span role="alert">
                    <strong class="text-danger h4">{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="group">
                <input type="submit" class="button" value="{{ __('Login') }}">
            </div>
            <div class="hr"></div>
            <div class="foot-lnk">
                <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
            </div>
            <input hidden name="total" value="{{$total}}" />
            <input hidden name="storeName" value="{{$storeName}}" />
            <input hidden name="orderNumber" value="{{$orderNumber}}" />
            <input hidden name="urlBack" value="{{$urlBack}}" />
            <input hidden name="storeAccountNumber" value="{{$storeAccountNumber}}" />
        </form>
    </div>
</div>
@endsection
