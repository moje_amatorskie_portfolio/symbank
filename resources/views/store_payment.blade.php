@extends('layouts.store_layout')

@section('box')
<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab text-center"></label>
<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab text-center"></label>
<div class="login-form">
    <div class="sign-in-htm text-center">
        <form method="POST" action="{{url('store_payment_authorise')}}" autocomplete="off">
            @csrf
            <input id="tab-1" type="radio" name="tab" class="sign-in" checked>
            <label for="tab-1" class="tab ">{{__('payment for order number')}} {{$orderNumber}}<br />
                {{__('from account number')}}:</label>
            <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab text-center"></label>
            <div class="group">
                <label for="username" class="label"></label>
                <input hidden id="username" type="text" class="input" name="username" value="111" autocomplete="off">
                <span role="alert">
                    <strong class="text-danger h4">{{ $message ?? "" }}</strong>
                </span>
            </div>

            <div class="row">
                <div class="col-2"></div>
                <div class="col-10 text-left">
                    <div class="group h6">
                        <input id="{{$accounts->first()->id}}" type="radio" class="check" checked name="account_id" value="{{$accounts->first()->id}}">
                        <label class="cursorPointer" for="{{$accounts->first()->id}}"><span class="icon"></span> {{$accounts->first()->account_number}} {{__('account balance')}} {{ $accounts->first()->account_balance }} €</label>
                        @foreach ($accounts as $account)
                        @if ($loop->first) @continue @endif
                        <input id="{{$account->id}}" type="radio" class="check" name="account_id" value="{{$account->id}}">
                        <label class='cursorPointer' for="{{$account->id}}">
                            <span class="icon"></span>
                            {{$account->account_number }} {{__('account balance')}} {{ $account->account_balance }} €
                        </label>
                        @endforeach
                    </div> <!-- group h5 -->
                </div> <!-- col-6-->
                <div class='col'></div>
            </div> <!-- ./row -->
            <input hidden name="total" value="{{$total}}" />
            <input hidden name="storeName" value="{{$storeName}}" />
            <input hidden name="orderNumber" value="{{$orderNumber}}" />
            <input hidden name="urlBack" value="{{$urlBack}}" />
            <input hidden name="storeAccountNumber" value="{{$storeAccountNumber}}" />
            <input hidden name="accounts" value="{{$accounts ?? ''}}" />
            <br>
            <div class="text-center m-2"><button class="btn btn-lg btn-warning pl-3 pr-3 font-weight-bold text-uppercase"> {{__('pay')}} {{$total}} €</button></div>
        </form>
    </div>
</div>
@endsection
