<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_name', 'account_number', 'account_balance', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
