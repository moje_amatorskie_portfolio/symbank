<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Payment;
use App\User;
use App\Deposit;
use App\Loan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NewLoanApiController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentUserId = Auth::id();
        $loanInterestRate= $request->loanInterestRate/100.0;
        $multiplier = 1.0 + $loanInterestRate/60.0;
        $loanDuration = $request->loanDuration;
        $d = new \DateTime();
        $d->modify("+$loanDuration minute");
        $loanEndPeriod = $d->format("Y-m-d H:i:s");
        $loanAmount= $request->loanAmount;
        $loanInitialBalance = $request->loanAmount;
        $loanName= $request->loanName;
        $accountNumber = $request->accountNumber;
        $accountBalance = Account::where('account_number', $accountNumber)->firstOrFail()->account_balance;
        $installmentPaymentDescription = __('installment payment');
        $lastRepaymentDescription = __('last repayment');
        $loan = new Loan();
        $loan->name = $loanName;
        $loan->initial_balance = $loanInitialBalance;
        $loan->balance = $loanAmount;
        $loan->interest_charged = 0;
        $loan->interest_rate = $loanInterestRate;
        $loan->loan_ends_at = $loanEndPeriod;
        $loan->user_id = $currentUserId;
        $loan->save();
        $accountObject = Account::where('account_number', $accountNumber)->firstOrFail();
        $accountObject->account_balance +=$loanAmount;
        $accountObject->save();
        $payment = new Payment();
        $payment->payment_amount = $loanAmount;
        $payment->description = $loanName;
        $payment->from_account_id = Account::where('account_number', 999999999999)->firstOrFail()->id;
        $payment->to_account_id = $accountObject->id;
        $payment->save();
        $intervalValue = $loanDuration;
        $loanEvent1 = "loan_event1_$loan->id";
        $loanEvent2 = "loan_event2_$loan->id";
        $loanEvent3 = "loan_event3_$loan->id";
        $loanEvent1Final = "loan_event1_final$loan->id";
        $loanEvent2Final = "loan_event2_final$loan->id";
        $loanRepaymentEvent1 = "loan_repayment_event1_$loan->id";
        $loanRepaymentEvent2 = "loan_repayment_event2_$loan->id";
        $instalment = $loanAmount/$intervalValue;
        $intervalValueMinusOne = $intervalValue-1;
        DB::unprepared("
            create event  if not exists $loanEvent1
            on schedule every 1 minute
            starts current_timestamp + interval 1 minute
            ends current_timestamp + interval $intervalValue minute
            do
            update loans set balance = balance * $multiplier
            where id = $loan->id;
        ");
        //wypłata raty z konta
        DB::unprepared("
            create event  if not exists $loanEvent2
            on schedule every 1 minute
            starts current_timestamp + interval 1 minute
            ends current_timestamp + interval $intervalValueMinusOne minute
            do
            update accounts set account_balance = account_balance - $instalment
            where account_number = $accountNumber;
            ");
        DB::unprepared("
            create event  if not exists $loanEvent3
            on schedule every 1 minute
            starts current_timestamp + interval 1 minute
            ends current_timestamp + interval $intervalValueMinusOne minute
            do
            update loans set balance = balance - $instalment
            where id = $loan->id;
        ");
        $symbankAccountId = Account::where('account_number', 999999999999)->firstOrFail()->id;
        DB::unprepared("
            create event  if not exists $loanRepaymentEvent1
            on schedule every 1 minute
            starts current_timestamp + interval 1 minute
            ends current_timestamp + interval $intervalValueMinusOne minute
            do
            insert into payments values(null,now(),now(),$instalment,'$installmentPaymentDescription',$accountObject->id,$symbankAccountId)
        ");
        $loanDurationPlusOne = $loanDuration+1;
        $loanDurationPlusTwo = $loanDuration+2;
        DB::unprepared("
            create event  if not exists $loanRepaymentEvent2
            on schedule at current_timestamp + interval $loanDuration minute
            do
            insert into payments values(null,now(),now(),(select balance from loans where id = $loan->id),'$lastRepaymentDescription',$accountObject->id,$symbankAccountId)
        ");
        //wypłata ostatniej raty wraz z naliczonymi odsetkami
        DB::unprepared("
            create event if not exists $loanEvent1Final
            on schedule at current_timestamp + interval $loanDurationPlusOne minute
            do
            update accounts set account_balance = account_balance - (select balance from loans where id = $loan->id)
            where id = $accountObject->id;
        ");
        DB::unprepared("create event  if not exists $loanEvent2Final
            on schedule at current_timestamp + interval $loanDurationPlusTwo minute
            do
            update loans set balance = 0
            where id = $loan->id;
        ");
        $alertMessage = __("loan opened successfully");
        $alertClass = "alert-success";
        return [$alertMessage,$alertClass];
    }
}
