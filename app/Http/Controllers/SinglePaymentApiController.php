<?php

namespace App\Http\Controllers;

use App\Account;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SinglePaymentApiController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $errors = array();
            if ($request->fromAccountNumber == $request->toAccountNumber) {
                $errors["errors"][] = __("you can't pay into the same account");
            }
            //tutaj nie dałem firstOrFail bo wyskakuje 404, zamiast treści błedu, że konto nie istnieje
            if (!Account::where('account_number', $request->toAccountNumber)->first()) {
                $errors["errors"][] = __("this account number doesn't exist");
            }
            if ($request->amount <= 0) {
                $errors["errors"][] = __("the amount must be positive number");
            }
            if ($request->amount > Account::where('account_number', $request->fromAccountNumber)->firstOrFail()->account_balance) {
                $errors["errors"][] = __("you don't have enough money to make this payment");
            }
            if ($errors) {
                return $errors;
            }
            DB::beginTransaction();
            $fromAccount = Account::where('account_number', $request->fromAccountNumber)->firstOrFail();
            $toAccount = Account::where('account_number', $request->toAccountNumber)->firstOrFail();
            $fromAccount->account_balance = $fromAccount->account_balance - $request->amount;
            $fromAccount->save();
            $toAccount->account_balance = $toAccount->account_balance + $request->amount;
            $toAccount->save();
            $payment = new Payment();
            $payment->payment_amount = $request->amount;
            $payment->from_account_id = Account::where('account_number', $request->fromAccountNumber)->firstOrFail()->id;
            $payment->to_account_id = Account::where('account_number', $request->toAccountNumber)->firstOrFail()->id;
            $payment->description = $request->description;
            $payment->save();
            DB::commit();
            $message = __("payment successful");
            return $message;
        }
    }
}
