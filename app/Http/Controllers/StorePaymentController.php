<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SinglePaymentApiController as ApiController;

class StorePaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(Request $request)
    {
        $total = $request->total;
        $storeName = $request->storeName;
        $orderNumber = $request->orderNumber;
        $urlBack = $request->urlBack;
        $storeAccountNumber = $request->storeAccountNumber;
        if (Auth::check()) {
            $accounts = $request->user()->accounts()->get();
            return view('store_payment', compact('total', 'storeName', 'orderNumber', 'urlBack', 'storeAccountNumber', 'message', 'accounts'));
        }
    }

    public function authorise(Request $request)
    {
        $total = $request->total;
        $storeName = $request->storeName;
        $orderNumber = $request->orderNumber;
        $urlBack = $request->urlBack;
        $accounts = $request->accounts;
        $accounts = $request->user()->accounts()->get();
        $accountId = $request->account_id;
        $storeAccountNumber = $request->storeAccountNumber;
        $toAccountNumber = $storeAccountNumber;
        $fromAccountNumber = Account::where('id', $accountId)->firstOrFail()->account_number;
        $amount = $total;
        $description = __('order number').' '.$orderNumber;
        $url = action('SinglePaymentApiController@store');
        $form_data = array(
            'toAccountNumber' => $toAccountNumber,
            'fromAccountNumber' => $fromAccountNumber,
            'amount' => $amount,
            'description' => $description,
        );
        $request->request->add($form_data);
        $ApiControllerObject = new ApiController();
        $response = $ApiControllerObject->store($request);
        if (is_array($response) && array_key_exists('errors', $response)) {
            $message = implode('<br />', $response['errors']);
            return view('store_payment', compact('total', 'storeName', 'storeAccountNumber', 'orderNumber', 'urlBack', 'accounts', 'message'));
        }
        $this->redirectBackToStore($request);
    }

    public function redirectBackToStore(Request $request)
    {
        $urlAway = $request->urlBack.'?'.'message='.__('payment successful');
        Auth::logout();
        header("Location: $urlAway");
        exit();
    }
}
