<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Payment;
use App\User;
use App\Deposit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NewDepositApiController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentUserId = Auth::id();
        $depositInterestRate= $request->depositInterestRate/100.0;
        $multiplier = 1.0+$depositInterestRate/60.0;
        $depositDuration = $request->depositDuration;
        $d = new \DateTime();
        $d->modify("+".$depositDuration." minute");
        $depositEndPeriod = $d->format("Y-m-d H:i:s");
        $depositRenewal= ($request->depositRenewal=="true") ? true : false;
        $depositAmount= $request->depositAmount;
        $depositInitialBalance = $request->depositAmount;
        $depositName= $request->depositName;
        $accountNumber = $request->accountNumber;
        $accountBalance = Account::where('account_number', $accountNumber)->firstOrFail()->account_balance;
        if ($depositAmount > $accountBalance) {
            $alertMessage = __("you don't have enough money to open this deposit");
            $alertClass = "alert-danger";
            return [$alertMessage, $alertClass];
        }
        $deposit = new Deposit();
        $deposit->name = $depositName;
        $deposit->initial_balance = $depositInitialBalance;
        $deposit->balance = $depositAmount;
        $deposit->accrued_interest = 0;
        $deposit->interest_rate = $depositInterestRate;
        $deposit->renewal = $depositRenewal;
        $deposit->deposit_ends_at = $depositEndPeriod;
        $deposit->user_id = $currentUserId;
        $deposit->save();
        //dla lokaty odnawialnej chciałem ustawić więcej, ale nie mogę ustawić więcej niż 200 miesięcy
        $eventName1 = "event1_$deposit->id";
        $eventName2 = "event2_$deposit->id";
        $intervalValue = $depositDuration;
        $depositDurationPlusOne = $depositDuration+1;
        if ($depositRenewal) {
            //dodać prepare() i exec()
            $intervalValue = 200;
            //nalicznie odsetek
            DB::unprepared("
                create event  if not exists $eventName1
                on schedule every 1 minute
                starts current_timestamp + interval 1 minute
                ends current_timestamp + interval $intervalValue minute
                do
                update deposits set balance = balance * $multiplier
                where id = $deposit->id;
            ");
            DB::unprepared("
                create event  if not exists $eventName2
                on schedule every $depositDuration minute
                starts current_timestamp + interval $depositDurationPlusOne minute
                ends current_timestamp + interval $intervalValue minute
                do
                update deposits set initial_balance = balance
                where id = $deposit->id;
            ");
        }
        if (!$depositRenewal) {
            DB::unprepared("
                create event  if not exists $eventName1
                on schedule every 1 minute
                starts current_timestamp + interval 1 minute
                ends current_timestamp + interval $depositDuration minute
                do
                update deposits set balance = balance * $multiplier
                where id = $deposit->id;
            ");
            //dopisanie odsetek do salda lokaty po skończeniu okresu lokaty
            DB::unprepared("
                create event  if not exists $eventName2
                on schedule at current_timestamp + interval $depositDurationPlusOne minute
                do
                update deposits set initial_balance = balance
                where id = $deposit->id;
            ");
        }
        $accountObject = Account::where('account_number', $accountNumber)->firstOrFail();
        $accountObject->account_balance -=$depositAmount;
        $accountObject->save();
        $payment = new Payment();
        $payment->payment_amount = $depositAmount;
        $payment->description = $depositName;
        $payment->from_account_id = $accountObject->id;
        $payment->to_account_id = $accountObject->id;
        $payment->save();
        $alertMessage = __("deposit opened successfully");
        $alertClass = "alert-success";
        return [$alertMessage,$alertClass];
    }
}
