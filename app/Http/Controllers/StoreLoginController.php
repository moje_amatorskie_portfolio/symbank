<?php

namespace App\Http\Controllers;

use App\Account;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StorePaymentController;
use App\Http\Controllers\LocalizationController;

class StoreLoginController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $total = $request->total;
        $storeName = $request->storeName;
        $orderNumber = $request->orderNumber;
        $urlBack = substr(url(''), 0, strlen(url(''))-15) .$request->urlBack;
        $storeAccountNumber = $request->storeAccountNumber;
        return view('auth.store_login', compact('total', 'storeName', 'orderNumber', 'urlBack', 'storeAccountNumber'));
    }

    public function authenticate(Request $request)
    {
        $total = $request->total;
        $storeName = $request->storeName;
        $orderNumber = $request->orderNumber;
        $urlBack = $request->urlBack;
        $storeAccountNumber = $request->storeAccountNumber;
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->route('create', $request);
        }
        $message = __("credentials don't match");
        return view('auth.store_login', compact('total', 'storeName', 'orderNumber', 'urlBack', 'message', 'storeAccountNumber'));
    }
}
