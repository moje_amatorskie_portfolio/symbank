<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Payment;
use App\User;
use App\Deposit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FirstAccountApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->accounts()->firstOrFail()->account_number;
    }
}
