<?php

namespace App\Http\Controllers;

use App\Account;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewBankAccountApiController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $countOfCurrentUserBankAccounts = auth()->user()->accounts()->count();
        $initialBalance = rand((8 - $countOfCurrentUserBankAccounts) * 100, (9 - $countOfCurrentUserBankAccounts) * 100);
        $alertMessage = __("you can't have more than 9 accounts");
        $alertClass = "alert-danger";
        if ($countOfCurrentUserBankAccounts < 9) {
            $account = Account::create(["account_name" => __("account no.")." ".($countOfCurrentUserBankAccounts + 1),"account_number" => mt_rand(111111111111, 999999999999),"account_balance" => $initialBalance,"user_id" => Auth::user()->id,]);
            $payment = new Payment();
            $payment->payment_amount = $initialBalance;
            $payment->description = "Initial balance";
            $payment->from_account_id = Account::where("account_number", 999999999999)->firstOrFail()->id;
            $payment->to_account_id = $account->id;
            $payment->save();
            $alertMessage = __("your account number"). $account->account_number .__("has been created")." &#128077;";
            $alertClass = "alert-success";
        }
        return json_encode([$alertMessage, $alertClass]);
    }
}
