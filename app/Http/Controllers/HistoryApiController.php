<?php

namespace App\Http\Controllers;

use App\Account;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HistoryApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //wybranie id pierwszego konta bankowego z listy kont, jeśli numer konta nie został przesłany, np. numer konta nie jest przesyłany gdy wybiera się `History` z głównego menu
        $BankAccountId = $request->user()->accounts()->firstOrFail()->id;
        //wybranie id przesłanego konta bankowego
        if ($request->bankAccountNumber != 0) {
            $BankAccountId = Account::where('account_number', $request->bankAccountNumber)->firstOrFail()->id;
        }
        $paymentsArray = DB::select('select * from payments where from_account_id = ? or  to_account_id= ? order by created_at desc', [$BankAccountId, $BankAccountId]);
        foreach ($paymentsArray as $singlePaymentObject) {
            $paymentDescription = $singlePaymentObject->description;
            //przypisanie różnych wartości zakłądając, że przelew jest przychodzący
            $paymentAmountColorClass = "col text-success";
            $paymentAmountSign = "+";
            if (!$paymentDescription) {
                $paymentDescription = __('Money In');
            }
            //przypisanie różnych wartości jeśli przelew jest wychodzący
            if ($singlePaymentObject->from_account_id == $BankAccountId) {
                $paymentAmountColorClass = "col text-danger";
                $paymentAmountSign = "-";
                if (!$paymentDescription) {
                    $paymentDescription = __('Money Out');
                }
            }
            $paymentsArrayToReturn["rows"][] = [
                "paymentId" => $singlePaymentObject->id,
                "date" => substr($singlePaymentObject->created_at, 0, 16),
                "payerName" => User::where('id', Account::where('id', $singlePaymentObject->from_account_id)->firstOrFail()->user_id)->firstOrFail()->name,"recipientName" => User::where('id', Account::where('id', $singlePaymentObject->to_account_id)->firstOrFail()->user_id)->firstOrFail()->name,
                "paymentAmount" => $singlePaymentObject->payment_amount,
                "paymentDescription" => $paymentDescription,
                "paymentAmountColorClass" => $paymentAmountColorClass,
                "paymentAmountSign" => $paymentAmountSign,
            ];
        }
        $accountBalance = Account::findOrFail($BankAccountId)->account_balance;
        $paymentsArrayToReturn["accountBalance"][] = $accountBalance;
        return $paymentsArrayToReturn;
    }
}
