<?php

use App\Account;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "Prestashop";
        $user->email = "prestashop@gmail.com";
        $user->password = bcrypt('555');
        $user->username = '555';
        $user->save();
        $account = new Account();
        $account->account_name = "Prestashop account";
        $account->account_number = "555555555555";
        $account->account_balance = 100000;
        $account->user_id = $user->id;
        $account->save();

        $user = new User();
        $user->name = "symBank";
        $user->email = "symbank@gmail.com";
        $user->password = bcrypt('999');
        $user->username = '999';
        $user->save();
        $account = new Account();
        $account->account_name = "symBank account";
        $account->account_number = "999999999999";
        $account->account_balance = 100000;
        $account->user_id = $user->id;
        $account->save();

        $user = new User();
        $user->name = "Donald Pump";
        $user->email = "donald.pump@gmail.com";
        $user->password = bcrypt('777');
        $user->username = '777';
        $user->save();
        $account = new Account();
        $account->account_name = "Donald's account";
        $account->account_number = "777777777777";
        $account->account_balance = 300000;
        $account->user_id = $user->id;
        $account->save();

        $user = new User();
        $user->name = "Ogrodniczy";
        $user->email = "sklep.ogrodniczy@gmail.com";
        $user->password = bcrypt('888');
        $user->username = '888';
        $user->save();
        $account = new Account();
        $account->account_name = "Konto Ogrodnika";
        $account->account_number = "888888888888";
        $account->account_balance = 700;
        $account->user_id = $user->id;
        $account->save();

        $user = new User();
        $user->name = "Jan Kowalski";
        $user->email = "jankowalski@gmail.com";
        $user->password = bcrypt('111');
        $user->username = '111';
        $user->save();
        $account = new Account();
        $account->account_name = "Konto Jana";
        $account->account_number = "133224354332";
        $account->account_balance = 23000;
        $account->user_id = $user->id;
        $account->save();
        $account = new Account();
        $account->account_name = "Drugie Konto Jana";
        $account->account_number = "637328284332";
        $account->account_balance = 32000;
        $account->user_id = $user->id;
        $account->save();

        $user = new User();
        $user->name = "Anna Nowak";
        $user->email = "annanowak@gmail.com";
        $user->password = bcrypt('222');
        $user->username = '222';
        $user->save();
        $account = new Account();
        $account->account_name = "Konto Anny";
        $account->account_number = "392722376534";
        $account->account_balance = 11000;
        $account->user_id = $user->id;
        $account->save();

        $user = new User();
        $user->name = "John Doe";
        $user->email = "johnDoe@gmail.com";
        $user->password = bcrypt('333');
        $user->username = '333';
        $user->save();
        $account = new Account();
        $account->account_name = "John's account";
        $account->account_number = "653323456764";
        $account->account_balance = 5000;
        $account->user_id = $user->id;
        $account->save();
    }
}
