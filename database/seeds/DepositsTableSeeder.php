<?php

use App\Account;
use App\Payment;
use App\User;
use App\Deposit;
use Illuminate\Database\Seeder;

class DepositsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deposit = new Deposit();
        $deposit->name = 'Deposit 1';
        $deposit->initial_balance = 1000;
        $deposit->balance = 1000;
        $deposit->accrued_interest = 0;
        $deposit->interest_rate = 0;
        $deposit->renewal = true;
        $endDate = new DateTime('next Month');
        $deposit->deposit_ends_at = $endDate->format('Y-m-d H:i:s');
        $deposit->user_id = 4;
        $deposit->save();

        $deposit = new Deposit();
        $deposit->name = 'Deposit 2';
        $deposit->initial_balance = 2000;
        $deposit->balance = 2000;
        $deposit->accrued_interest = 0;
        $deposit->interest_rate = 0;
        $deposit->renewal = false;
        $endDate = new DateTime('next Year');
        $deposit->deposit_ends_at = $endDate->format('Y-m-d H:i:s');
        $deposit->user_id = 4;
        $deposit->save();
    }
}
