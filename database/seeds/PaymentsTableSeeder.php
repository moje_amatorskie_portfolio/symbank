<?php

use App\Account;
use App\Payment;
use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account_1 = Account::where('account_number', '=', '777777777777')->first();
        $account_2 = Account::where('account_number', '999999999999')->first();
        $payment = new Payment();
        $payment->payment_amount = 100000;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'initial balance';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '999999999999')->first();
        $account_2 = Account::where('account_number', '888888888888')->first();
        $payment = new Payment();
        $payment->payment_amount = 700;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'initial balance';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 131;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 450;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'X Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 444;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'R Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 342;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 125;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'X Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 444;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'R Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 43;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 56;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 34;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '392722376534')->first();
        $account_2 = Account::where('account_number', '133224354332')->first();
        $payment = new Payment();
        $payment->payment_amount = 23;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'Z Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 45;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 45;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 77;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 17;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 13;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 44;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 223;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '653323456764')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 67;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'D Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '653323456764')->first();
        $payment = new Payment();
        $payment->payment_amount = 45;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'B Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 67;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '392722376534')->first();
        $account_2 = Account::where('account_number', '133224354332')->first();
        $payment = new Payment();
        $payment->payment_amount = 67;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'Z Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 150;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 450;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'X Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 444;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'R Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 130;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '392722376534')->first();
        $account_2 = Account::where('account_number', '133224354332')->first();
        $payment = new Payment();
        $payment->payment_amount = 101;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'Z Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 177;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 112;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 157;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 176;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 112;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '653323456764')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 278;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'D Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '653323456764')->first();
        $payment = new Payment();
        $payment->payment_amount = 32;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'B Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 450;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'X Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 131;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 178;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '653323456764')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 200;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'D Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '653323456764')->first();
        $payment = new Payment();
        $payment->payment_amount = 300;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'B Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 187;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '392722376534')->first();
        $account_2 = Account::where('account_number', '133224354332')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'Z Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 450;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'X Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 444;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'R Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 212;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'K Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 111;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'J Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '392722376534')->first();
        $account_2 = Account::where('account_number', '133224354332')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'Z Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 189;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 116;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '653323456764')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 10;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'D Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '653323456764')->first();
        $payment = new Payment();
        $payment->payment_amount = 300;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'B Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 450;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'X Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 444;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'R Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 145;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '637328284332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '392722376534')->first();
        $account_2 = Account::where('account_number', '133224354332')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'Z Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 175;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 133;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 100;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 15;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'A Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '653323456764')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 200;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'D Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '653323456764')->first();
        $payment = new Payment();
        $payment->payment_amount = 300;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'B Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 400;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'F payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '653323456764')->first();
        $account_2 = Account::where('account_number', '392722376534')->first();
        $payment = new Payment();
        $payment->payment_amount = 500;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'E Payment';
        $payment->save();

        $account_1 = Account::where('account_number', '=', '133224354332')->first();
        $account_2 = Account::where('account_number', '653323456764')->first();
        $payment = new Payment();
        $payment->payment_amount = 600;
        $payment->from_account_id = $account_1->id;
        $payment->to_account_id = $account_2->id;
        $payment->description = 'C Payment';
        $payment->save();
    }
}
