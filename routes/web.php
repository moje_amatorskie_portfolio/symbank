<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::match(['get', 'post'], '/api/accounts', 'AccountsListingApiController@index')->middleware('auth');
Route::match(['get', 'post'], '/api/history', 'HistoryApiController@index')->middleware('auth');
Route::match(['get', 'post'], '/api/deposits', 'DepositsApiController@index')->middleware('auth');
Route::match(['get', 'post'], '/api/payment', 'SinglePaymentApiController@store');
Route::match(['get', 'post'], '/api/new_account', 'NewBankAccountApiController@store')->middleware('auth');
Route::match(['get', 'post'], '/api/new_deposit', 'NewDepositApiController@store')->middleware('auth');
Route::match(['get', 'post'], '/api/new_loan', 'NewLoanApiController@store')->middleware('auth');
Route::match(['get', 'post'], '/api/first_account', 'FirstAccountApiController@index')->middleware('auth');
Route::get('/lang/{locale}', 'LocalizationController@changeLocale');
Route::match(['get','post'], '/store_login', 'StoreLoginController@index');
Route::match(['get','post'], '/store_payment_authenticate', 'StoreLoginController@authenticate');
Route::match(['get','post'], '/store_payment_create', 'StorePaymentController@create')->name('create');
Route::match(['get','post'], '/store_payment_authorise', 'StorePaymentController@authorise')->name('authorise');
Route::match(['get','post'], '/redirect_back_to_store', 'StorePaymentController@redirectBackToStore');
